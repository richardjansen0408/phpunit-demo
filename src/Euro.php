<?php

namespace Demo;


class Euro
{
    public function eurocentToCurrency($eurocent): string
    {
        $euros = $eurocent / 100;

        $money = number_format($euros, 2, ",", ".");

        $currency = "€ " . $money;

        return $currency;
    }
}
