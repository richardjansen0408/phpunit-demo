<?php
/**
 * Created by PhpStorm.
 * User: richard
 * Date: 10-04-18
 * Time: 14:51
 */

namespace Demo;

class Service
{
    private $client;

    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    public function getValueFromDependency()
    {
        return $this->client->getValue();
    }

    public function run()
    {
        $serverResponse = $this->client->getServerResponse();
        return "Server is used to generate this string: $serverResponse";
    }
}