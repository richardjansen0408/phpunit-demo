<?php
/**
 * Created by PhpStorm.
 * User: richard
 * Date: 10-04-18
 * Time: 14:51
 */

namespace Demo;

class Calculator
{

    public function add($a, $b)
    {
        return $a + $b;
    }

    public function multiply($a, $b)
    {
        return $a * $b;
    }

    public function divide($a, $b)
    {
        if (0 === $b) {
            throw new \Exception("Delen door 0 mag niet");
        }

        return $a / $b;
    }
}