<?php
/**
 * Created by PhpStorm.
 * User: richard
 * Date: 12-04-18
 * Time: 14:01
 */

namespace Demo;

class Saver
{

    protected $pdo;

    public function __construct(\PDO $pdo)
    {
        $this->pdo = $pdo;
    }

    public function save(string $table, array $data)
    {
        $sql = "INSERT INTO items(name) VALUES (:name)";

        $stmt = $this->pdo->prepare($sql);

        return $stmt->execute($data);

    }
}