<?php
/**
 * Created by PhpStorm.
 * User: richard
 * Date: 10-04-18
 * Time: 14:51
 */

namespace Demo;

class Client
{
    public function getServerResponse()
    {
        $isServerLive = (bool)random_int(0, 1);

        if ($isServerLive) {
            return 'value from live server';
        }
    }
}