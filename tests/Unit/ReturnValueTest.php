<?php
/**
 * Created by PhpStorm.
 * User: richard
 * Date: 10-04-18
 * Time: 13:59
 */

namespace Tests\Unit;

use PHPUnit\Framework\TestCase;
use Demo\Calculator;

class ReturnValueTest extends TestCase
{
    /**
     * @var De class die we gaan testen
     */
    private static $myCalculator;

    /**
     * PHPUnit setUp voor elke test
     */
    public function setUp()
    {
        self::$myCalculator = new Calculator;
    }

    /**
     * De test die $actual returnd
     */
    public function testOptellen()
    {
        $expected = 3;
        $actual   = self::$myCalculator->add(1, 2);

        $this->assertEquals($expected, $actual);

        return $actual;
    }

    /**
     * Test is afhankelijk van de return value van testOptellen
     *
     * @depends testOptellen
     */
    public function testIsHetEenGetal($result)
    {
        $this->assertTrue(is_numeric($result));
    }

}
