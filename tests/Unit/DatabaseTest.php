<?php
/**
 * Created by PhpStorm.
 * User: richard
 * Date: 10-04-18
 * Time: 13:59
 */

namespace Tests\Unit;

use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\YamlDataSet;


class DatabaseTest extends TestCase
{

    use TestCaseTrait;

    protected $conn;

    private $pdo;

    final public function getConnection()
    {
        if ($this->conn === null) {
            if ($this->pdo === null) {
                $dsn = "mysql:dbname=testdatabase;host=127.0.0.1:3306";
                $this->pdo = new \PDO($dsn, "root", "root");
            }
            $this->conn = $this->createDefaultDBConnection($this->pdo);
        }

        return $this->conn;
    }

    protected function getDataSet()
    {
        return new YamlDataSet(dirname(__FILE__) . "/dataset.yml");
    }

    public function testIsRowInsert()
    {
        $saver = new \Demo\Saver($this->pdo);

        $saver->save('items', ['name' => 'Foo']);

        $expecting = [0 => ['name' => 'Foo']];

        $stmt = $this->pdo->prepare("SELECT name FROM items WHERE name = :name");
        $stmt->execute(['name' => 'Foo']);
        $rows = $stmt->fetchAll();

        $this->assertArraySubset($expecting, $rows);
    }

}
