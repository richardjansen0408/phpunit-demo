<?php
/**
 * Created by PhpStorm.
 * User: richard
 * Date: 10-04-18
 * Time: 13:59
 */

namespace Tests\Unit;

use PHPUnit\Framework\TestCase;

class MoreTest extends TestCase
{

    public function testEquals()
    {
        $this->assertEquals(1, true); // succes
//        $this->assertSame(1, true); // fail
    }

    public function testBools()
    {
        $this->assertTrue(true);
        $this->assertFalse(false);
    }

    public function testCount()
    {
        $array = ['foo', 'bar', 'baz'];

        $this->assertCount(3, $array);
    }

    public function testSkipThisTest()
    {
        $this->markTestSkipped("Skip this test");
        $this->assertEquals('Never executed', 'Never executed');
    }



}
