<?php

namespace Tests\Unit;

use PHPUnit\Framework\TestCase;
use Demo\Euro;

class DataProviderTest extends TestCase
{

    /**
     * @dataProvider dataProvider
     */
    public function testCasting($expected, $eurocent)
    {
        $actual = (new Euro)->eurocentToCurrency($eurocent);

        $this->assertEquals($expected, $actual);
    }

    /**
     * Alle mogelijke scenario's om te testen.
     *
     * Elke array in deze array is een aparte test.
     *
     * Het zijn de params voor de test method
     *
     * @return array
     */
    public function dataProvider()
    {
        return [
            ['€ 1.000,00', 100000],
            [    '€ 1,00',    100],
            [    '€ 0,10',     10],
            [   '€ 25,00',   2500],
            [   '€ 49,95',   4995],
            [    '€ 0,00',      0],
            [    '€ 0,01',      1],
        ];
    }
}
