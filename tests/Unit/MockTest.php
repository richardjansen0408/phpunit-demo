<?php
/**
 * Created by PhpStorm.
 * User: richard
 * Date: 10-04-18
 * Time: 13:59
 */

namespace Tests\Unit;

use Demo\Client;
use Demo\Service;
use PHPUnit\Framework\TestCase;

class MockTest extends TestCase
{
    private static $myClassToTest;

    public function setUp()
    {
        /**
         * Client is een class met onvoorspelbaar gedrag
         *
         * Deze class is nodig voor het testen van Service
         *
         * Want Service is de class die we daadwerkelijk gaan testen
         */
        $client = new Client();


        // Maak een mock dat Client simuleert
        $client = $this
            ->getMockBuilder(\Demo\Client::class)
            ->setMethods(['getServerResponse'])
            ->getMock();

        $client
            ->method('getServerResponse')
            ->willReturn('value from live server');


        self::$myClassToTest = new Service($client);
    }

    public function testClassWithDependency()
    {
        $expected = "Server is used to generate this string: value from live server";

        $actual = self::$myClassToTest->run();

        $this->assertEquals($expected, $actual);
    }

}
