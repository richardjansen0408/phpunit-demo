<?php
/**
 * Created by PhpStorm.
 * User: richard
 * Date: 10-04-18
 * Time: 13:59
 */

namespace Tests\Unit;

use PHPUnit\Framework\TestCase;
use Demo\Calculator;

class ClassTest extends TestCase
{
    /**
     * @var De class die we gaan testen
     */
    private static $myCalculator;

    /**
     * PHPUnit setUp voor elke test
     */
    public static function setUpBeforeClass()
    {
        self::$myCalculator = new Calculator;
    }

    /**
     * De test
     */
    public function testOptellen()
    {
        $expected = 3;
        $actual   = self::$myCalculator->add(1, 2);

        $this->assertEquals($expected, $actual);
    }

    public function testVermenigvuldigen()
    {
        $actual = self::$myCalculator->multiply(1, 2);

        $this->assertEquals(2, $actual);
    }

    public function testDivisionByZero()
    {
        $this->expectException(\Exception::class);

        self::$myCalculator->divide(4, 0);
    }
}
