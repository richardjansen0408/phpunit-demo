<?php
/**
 * Created by PhpStorm.
 * User: richard
 * Date: 10-04-18
 * Time: 13:59
 */

namespace Tests\Unit;

use PHPUnit\Framework\TestCase;

class BasicTest extends TestCase
{
    /**
     * Een test method begint altijd met 'test'
     * Dan een veelzeggende naam dat de test goed beschrijft
     */
    public function testIsTrueOokEchtWelTrue()
    {
        /**
         * Een assertion test of jouw code iets terug geeft wat je verwacht
         *
         * Ik verwacht dat $actual gelijk is aan true
         */
        $actual = false;

        $this->assertTrue($actual); // success
    }
}
