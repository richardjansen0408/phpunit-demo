<?php
/**
 * Created by PhpStorm.
 * User: richard
 * Date: 13-04-18
 * Time: 09:28
 */

namespace Unit;


class Intro
{
    public $welcome = "
    
        --- Unit Testing met PHPUnit ---
       
        --------------------------------
        
        F5 of ⌘R overbodig.
        
        Voor automatisch testen van code.
        
        Helpt bij het ontwikkelen van nieuwe functionaliteit.
        
        Helpt ook bij refactoren van bestaande code.
        
        Helpt je tot nadenken en documenteren van alle mogelijke scenario's.
        
        Zorgt dat andere code niet omvalt, want die wordt ook getest.
        
        Zorgt uiteindelijk voor solide en geteste codebase.
    
    ";
}